import glob
import os
from bs4 import BeautifulSoup
from alphabet_detector import AlphabetDetector


indir = 'jsc_print_191k_20181227'
html_docs = glob.glob(os.path.join(indir, '*'))
outdir = indir + '_plain'
alpha_det = AlphabetDetector()
if not os.path.exists(outdir):
	os.mkdir(outdir)
for i, doc in enumerate(html_docs):
	print('process ', doc)
	# out_file = "./jsc_plain_pages_100k/jsc_{}.txt".format(str(i).zfill(5))
	file_name = os.path.basename(doc)
	if 'nohup' in file_name: 
		continue
	outfile = os.path.join(outdir, file_name)
	file_writer = open(outfile, mode='w')
	text = open(doc).read()
	if not text:
		continue
	try: 
		soup = BeautifulSoup(text, 'html.parser')
		# title = soup.find(attrs={'id': "lblHeadLine"}).get_text().strip()
		# file_writer.write(title + "\n")
		# date = soup.find(attrs={'id': "lblPublishDate"}).get_text().strip()
		# file_writer.write(date + "\n")
		# body = soup.find(attrs={'class': "PDA_jazeera_Body"}).get_text()
		# lines = [s.rstrip() for s in body.split("\n") if s.rstrip()]
		clean_text = soup.get_text()
		lines = clean_text.splitlines()
		for line in lines:
			if line.strip() and 'ARABIC' in alpha_det.detect_alphabet(line):
				file_writer.write(line + "\n")
	except BaseException as error:
		print('error: {}'.format(error))

