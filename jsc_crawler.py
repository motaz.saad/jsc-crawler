import sys

import requests
from bs4 import BeautifulSoup
from datetime import datetime
from requests.exceptions import RequestException

print_links = set()
crawled_links = set()
time_stamp = datetime.now().strftime('%Y%m%d_%H%M')
url_outfile = 'jsc_urls_{}.list'.format(time_stamp)
url_writer = open(url_outfile, mode='a')

jsc_news_home_url = 'https://www.aljazeera.net/news/'

# the default recursion limit is 1000. It is a guard against a stack overflow
sys.setrecursionlimit(5000)  # doing so is dangerous.


def crawl_links(web_url, stop=500000):
    sys.stdout.write("\rprint_links: {0}\t crawled_links: {1}".
                     format(len(print_links), len(crawled_links)))
    sys.stdout.flush()
    if len(print_links) > stop:
        return
    else:
        try:
            html_doc = requests.get(web_url).text
            soup = BeautifulSoup(html_doc, 'html.parser')
            links = soup.find_all('a')
            for link in links:
                my_link = str(link.get('href'))
                # print('======================')
                # print('link:', link)
                # print('======================')
                # print('my_link:', my_link)
                # print('======================')
                if 'home/print/' in my_link:
                    print_links.add(my_link)
                    url_writer.write(my_link + '\n')
                elif '/news/' in my_link:
                    if 'aljazeera.net' not in my_link:
                        target_link = 'https://www.aljazeera.net' + my_link
                        target_link = target_link.strip()
                    else:
                        target_link = my_link
                    if target_link not in crawled_links:
                        # print('\ncrawling {}'.format(target_link))
                        crawled_links.add(target_link.strip())
                        crawl_links(target_link.strip())

        except RequestException as error:
            print('\nerror: {}\n'.format(error))
        except UnicodeError as error:
            print('\nerror: {}\n'.format(error))


if __name__ == '__main__':
    try:
        crawl_links(jsc_news_home_url)
    except Exception as err:
        print('\nerror: {}'.format(err))
    # finally:
    # print('\nwriting urls')
    # time_stamp = datetime.now().strftime('%Y%m%d_%H:%M')
    # url_outfile = 'jsc_urls_{}.txt'.format(time_stamp)
    # url_writer = open(url_outfile, mode='a')
    # url_writer.write('\n'.join(print_links))
    print('\nall done!\n')
